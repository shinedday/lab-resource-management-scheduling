"""
Instance class file
"""
from typing import List

import copy

from instance.job import Job
from instance.serverdata import ServerData


class Instance:
    """
    Instance class, convert txt input file into data
    """

    def __init__(self, path: str) -> None:

        with open(path, "r", encoding="utf-8") as f:
            lines = f.readlines()

        for line in lines:
            if line.startswith("job_file"):
                self.job_file = line.split(" = ")[1][1:-2]
            elif line.startswith("server_file"):
                self.server_file = line.split(" = ")[1][1:-2]
            elif line.startswith("dependency_file"):
                self.dependency_file = line.split(" = ")[1][1:-2]
            elif line.startswith("power_cap"):
                self.power_cap = int(line.split(" = ")[1][:-1])
            elif line.startswith("energy_cap"):
                self.energy_cap = int(line.split(" = ")[1][:-1])
            elif line.startswith("repeat"):
                self.repeat = int(line.split(" = ")[1][:-1])
        self.jobs: List[Job] = []
        self.expended_jobs: List[Job] = []
        self.servers: List[ServerData] = []

        self.__txt_server_to_class()
        self.__txt_jobs_to_class()
        self.__txt_dep_to_class()
        self.__raw_jobs_to_jobs()

    def __txt_jobs_to_class(self) -> None:
        """
        Read job file to create job object
        :return: None
        """
        with open(self.job_file, "r", encoding="utf-8") as f:
            lines = f.readlines()
        for raw_jobs in lines[1:]:
            if raw_jobs[0] != "\n":
                self.jobs.append(Job(raw_jobs))

    def __txt_dep_to_class(self) -> None:
        """
        Read dependency file and add them to the jobs
        :return: None
        """
        with open(self.dependency_file, "r", encoding="utf-8") as f:
            lines = f.readlines()
        for raw_deps in lines[1:]:
            if raw_deps[0] != "\n":
                dep_from = raw_deps[0]
                dep_to = raw_deps[4]

                self.jobs[int(dep_to)].add_dependency(int(dep_from))

    def __txt_server_to_class(self) -> None:
        """
        Read server file and create server object
        :return: None
        """
        with open(self.server_file, "r", encoding="utf-8") as f:
            lines = f.readlines()
        for raw_server in lines[1:]:
            if raw_server[0] != "\n":
                self.servers.append(ServerData(raw_server))

    def __raw_jobs_to_jobs(self) -> None:
        """
        Transform repeatable jobs into list of jobs
        :return: None
        """

        for job in self.jobs:

            self.expended_jobs.append(job)

            if job.period != 0:
                for repeat in range(1, self.repeat+1):
                    new_job = copy.copy(job)
                    # new_job.identifier = new_job.identifier + float(repeat+1)/(10**int(len(str(self.repeat-1))))
                    new_job.arrival = new_job.arrival + repeat * new_job.period
                    new_job.end = new_job.end + repeat * new_job.period
                    new_job.identifier += 100 * repeat
                    self.expended_jobs.append(new_job)

    def __repr__(self) -> str:
        res = "[FILE] :\n"
        res += " * job_file -> " + self.job_file + "\n"
        res += " * server_file -> " + self.server_file + "\n"
        res += " * dependency_file -> " + self.dependency_file + "\n"
        res += "----------\n"
        res += "[PARAMS]\n"
        res += " * power_cap -> " + str(self.power_cap) + "\n"
        res += " * energy_cap -> " + str(self.energy_cap) + "\n"
        res += " * repeat -> " + str(self.repeat) + "\n"
        res += "----------\n"
        res += "[SERVERS]\n"
        for serv in self.servers:
            res += serv.__repr__()
        res += "----------\n"
        res += "[JOBS]\n"
        for job in self.expended_jobs:
            res += job.__repr__()
        return res
