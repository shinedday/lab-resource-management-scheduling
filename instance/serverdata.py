"""
Server class file
"""


class ServerData:
    """
    Server class  data
    """

    def __init__(self, raw_data: str) -> None:
        identifier = int(raw_data[0])

        freq = [int(i) for i in raw_data.split("(")[1].split(")")[0].split(" ")]

        self.identifier = identifier
        self.freq = freq

    def __repr__(self) -> str:
        res = "Server : " + str(self.identifier) + " with freq : "
        for f in self.freq:
            res += str(f) + " "
        return res + "\n"
