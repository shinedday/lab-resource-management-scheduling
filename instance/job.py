"""
Job class file
"""


class Job:
    """
    Job class
    """

    def __init__(self, raw_data: str) -> None:
        raw_int = [int(i) for i in raw_data.split(" ")]

        self.identifier = raw_int[0]
        self.arrival = raw_int[1]
        self.unit = raw_int[2]
        self.end = raw_int[3] + self.arrival
        self.period = raw_int[4]
        self.dependency = []

        self.unit_left = self.unit

    def add_dependency(self, dependency: int) -> None:
        """
        :param dependency: Another Job ID
        :return: None
        """
        self.dependency.append(dependency)

    def __repr__(self) -> str:
        res = "Job : " + str(self.identifier) + "\n" + \
              " * Date : " + str(self.arrival) + " -> " + str(self.end) + "\n" + \
              " * workload : " + str(self.unit) + " | period : " + str(self.period) + "\n" + \
              " * dependencie : "
        for dep in self.dependency:
            res += str(dep) + " "
        return res + "\n\n"
