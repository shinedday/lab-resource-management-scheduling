from typing import List

from server.server import Server
from server.state import State


class RrServer(Server):
    """
    RoundRobin
    """

    def __init__(self, identifier: int, frequencies: List[int], scheduler):
        super().__init__(identifier, frequencies, scheduler)

        self.id_job_done: List[int] = []

    def on_decide(self) -> None:

        if len(self.remaining_jobs) == 0:
            self.state = State.IDLE
            self.next_job_id = -1
            return

        self.state = State.WORKING

        job_to_do = self.lasting_jobs()

        if len(job_to_do) == 0:
            self.next_job_id = 0
            self.id_job_done = [self.remaining_jobs[0].identifier]
        else:
            self.next_job_id = job_to_do[0]
            self.id_job_done.append(self.remaining_jobs[self.next_job_id].identifier)

    def lasting_jobs(self) -> List[int]:

        result: List[int] = []

        for job_id in range(len(self.remaining_jobs)):
            if not self.remaining_jobs[job_id].identifier in self.id_job_done:
                result.append(job_id)

        return result
