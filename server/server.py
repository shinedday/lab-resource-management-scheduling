from math import ceil
from typing import List

from instance.job import Job
from scheduler.outputdata import OutputData
from server.state import State


class Server:
    """
    Class server to overwrite with the wanted scheduling algorithm
    """

    def __init__(self, identifier: int, frequencies: List[int], scheduler):
        self.remaining_jobs: List[Job] = []
        self.identifier: int = identifier
        self.frequencies = frequencies

        self.pmax = 200

        self.freq = 0

        self.state = State.IDLE

        self.next_job_id: int = -1

        self.scheduler = scheduler

        self.time_worked = 0
        self.amount_of_work = 0
        self.deadline_missed = 0
        self.energy = 0
        self.maxpower = 0

    def on_decide(self) -> None:
        """
        TO OVERWRITE
        Choose what's the next job to execute
        :return: None
        """
        pass

    def on_decide_freq(self) -> None:
        """
        Decide the right freq
        :return: None
        """

        if len(self.remaining_jobs) > 0:
            self.freq = self.choose_frequencyV3()
        else:
            self.freq = 0

    def choose_frequencyV2(self) -> int:
        """
        choose what's the best frequency to use for the next task, limiting power consumption
        :return: return the frequency to use
        """

        for freq in self.frequencies:
            if self.can_slowdown(freq):
                return freq
        return self.frequencies[-1]

    def choose_frequencyV3(self) -> int:
        """
        choose what's the best frequency to use for the next task, limiting power consumption
        :return: return the frequency to use
        """
        if self.next_job_id == -1:
            return self.frequencies[0]

        for freq_id in range(len(self.frequencies)):

            if self.can_slowdownV2(freq_id):
                return self.frequencies[freq_id]

        freq_id = len(self.frequencies) - 1
        while self.frequencies[freq_id] >= self.remaining_jobs[self.next_job_id].unit_left and freq_id > 0:
            freq_id -= 1
        return self.frequencies[freq_id]

    def can_slowdownV2(self, freq_id: int) -> bool:

        self.remaining_jobs[self.next_job_id].unit_left -= self.frequencies[freq_id]
        for freq_id_test in range(0, freq_id + 1):
            is_possible = self.will_miss_deadline(
                self.frequencies[freq_id_test],
                sorted(self.remaining_jobs, key=lambda x: x.end),
                self.scheduler.current_time + 1
            )

            if is_possible:
                self.remaining_jobs[self.next_job_id].unit_left += self.frequencies[freq_id]
                return True

        self.remaining_jobs[self.next_job_id].unit_left += self.frequencies[freq_id]
        return False

    def will_miss_deadline(self, freq, jobs, time):
        for job in jobs:
            time += ceil(job.unit_left / freq)
            if time > job.end:
                return False
        return True

    def on_act(self) -> OutputData:
        """
        Execute the job, delete it if the job is done
        :return: OutputData, a formatted way to know the job done
        """

        if len(self.remaining_jobs) == 0:
            return None

        result = OutputData(
            self.remaining_jobs[self.next_job_id].identifier,
            self.identifier,
            self.scheduler.current_time,
            self.scheduler.current_time + 1,
            self.freq,
            self.pmax * (self.freq / self.frequencies[-1]) * (self.freq / self.frequencies[-1])
        )

        self.energy += self.get_power()
        if self.get_power() > self.maxpower:
            self.maxpower = self.get_power()

        self.time_worked += 1
        self.amount_of_work += self.freq

        if self.remaining_jobs[self.next_job_id].end < self.scheduler.current_time:
            self.deadline_missed += 1

        self.remaining_jobs[self.next_job_id].unit_left -= self.freq

        if self.remaining_jobs[self.next_job_id].unit_left <= 0:
            del self.remaining_jobs[self.next_job_id]

        if len(self.remaining_jobs) <= 0:
            self.state = State.IDLE

        return result

    def score(self) -> int:
        """
        :return: an integer representing how likely this server want a new job
        """

        score = 0
        relative_time = self.scheduler.current_time

        job_to_do = sorted(self.remaining_jobs, key=lambda x: x.end, reverse=True)

        for job in job_to_do:
            relative_time += job.unit_left / self.frequencies[-1]

            if relative_time > job.end:
                score += 1000
            score += job.unit_left

        return score

    def add_job(self, job: Job) -> None:
        """
        :param job: job to add to the remaining job
        :return: None
        """
        self.remaining_jobs.append(job)

    def can_slowdown(self, freq: int) -> bool:

        relative_time = self.scheduler.current_time + 1

        self.remaining_jobs[self.next_job_id].unit_left -= freq

        job_to_do = sorted(self.remaining_jobs, key=lambda x: x.end, reverse=True)

        for job in job_to_do:
            relative_time += ceil(job.unit_left / self.frequencies[-1])

            if relative_time > job.end:
                self.remaining_jobs[self.next_job_id].unit_left += freq
                return False

        self.remaining_jobs[self.next_job_id].unit_left += freq
        return True

    def can_shut_down(self) -> int:
        """
        Compute if a server could not work for this quantum if it's needed
        :return:
        """
        if len(self.remaining_jobs) == 0:
            return 0

        if self.can_slowdown(0):
            return 0

        return self.score()

    def get_power(self) -> float:
        return self.pmax * (self.freq / self.frequencies[-1]) * (self.freq / self.frequencies[-1])
