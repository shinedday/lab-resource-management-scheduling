from typing import List

from server.server import Server
from server.state import State


class RmsServer(Server):

    def __init__(self, identifier: int, frequencies: List[int], scheduler):
        super().__init__(identifier, frequencies, scheduler)

    def on_decide(self) -> None:

        if len(self.remaining_jobs) == 0:
            self.state = State.IDLE
            self.next_job_id = -1
            return

        self.next_job_id = 0
        self.state = State.WORKING
        for job_id in range(len(self.remaining_jobs)):
            if self.remaining_jobs[job_id].period < self.remaining_jobs[self.next_job_id].period:
                self.next_job_id = job_id
