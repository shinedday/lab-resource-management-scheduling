"""
Server state
"""
from enum import Enum, auto


class State(Enum):
    """
    Don't have any remaining job
    """
    IDLE = auto()
    """
    Currently working
    """
    WORKING = auto()
