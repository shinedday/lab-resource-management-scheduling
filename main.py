from instance.instance import Instance
from scheduler.algo import Algo
from scheduler.scheduler import Scheduler

print("Creating Instance ..")

instance = Instance("test1.txt")

# print(instance)

print("Creating Scheduler ..")
scheduler = Scheduler(
    instance.expended_jobs,
    scheduling_algo=Algo.EDF,
    servers=instance.servers,
    pmax=instance.power_cap,
    emax=instance.energy_cap,
    path="test.txt"
)

print("Running the scheduler ..")
scheduler.run()
scheduler.print_report()

print("Printing result")
scheduler.write()

scheduler.plot_power()
scheduler.plot_energy()
