# **Scheduling - Resource management for Embedded systems**

**Names :** Sebastien GOYON, Alan MACE  
**Group :** M1 CSA  
**Git link :** https://gitlab.com/shinedday/lab-resource-management-scheduling  

## **Table of content**

- [**Scheduling - Resource management for Embedded systems**](#scheduling---resource-management-for-embedded-systems)
  - [**Table of content**](#table-of-content)
  - [**Context**](#context)
  - [**Usage :**](#usage-)
  - [**Object model of the labwork**](#object-model-of-the-labwork)
  - [**Scheduling Cycle Explained**](#scheduling-cycle-explained)
    - [**Stop condition**](#stop-condition)
    - [**Part 1 : Scheduler work**](#part-1--scheduler-work)
    - [**Part 2 : Server decide**](#part-2--server-decide)
    - [**Part 3 : Power check**](#part-3--power-check)
    - [**Part 4 : Server work**](#part-4--server-work)
  - [**Power management**](#power-management)
  - [**Metrics**](#metrics)
  - [**Result**](#result)
    - [**FIFO**](#fifo)
    - [**Round Robin**](#round-robin)
    - [**RMS**](#rms)
    - [**EDF**](#edf)
    - [**Comparison**](#comparison)
    - [**Power management test**](#power-management-test)

## **Context**

The programing language chosen for the project is Python3.

The scheduler is made using few assumptions : 
 * There are no time or resources cost constraints (so code complexity isn't an issue)
 * The scheduler will be implemented using timesteps of 1, it's not an event based scheduler.
 * The scheduler will not be aware of future jobs so all the optimization will be done according to the job that have already arrived.
 * The scheduling will not be global : Job will be assigned to a specific server and won't be able to change afterwards.
 * No job dependencies will be taken into account.

Also, the algorithms will be implemented using their definition :
 * FIFO : The highest priority is the arrival date
 * RoundRobin : All the jobs remaining will be executed one after another during 1 tick (1 unit of time)
 * RMS : The lowest period have the lowest priority, job without period have higher priority than job with periods
 * EDF : The earliest deadline have the highest priority.

Note : The repeat value in instance is the number of time we'll see a job come back, if repeat = 0 then all job are executed once, repeat = 1 (all job with period are executed once more)

## **Usage :**

The main file is main.py, inside this file you can choose the scheduling algorith and the instance file.

## **Object model of the labwork**

```mermaid
classDiagram

    class State_Enum{
        IDLE
        WORKING
    }

    class Server {
        int identifier
        List~int~ frequencies
        List~Job~ pending_jobs

        on_decide()
        on_act() Data

        score() int
        add_job(~job~)
    }

    class FIFOServer{
        on_decide()
    }

    
    class EDFServer{
        on_decide()
    }
    
    class RMSServer{
        on_decide()
    }
    
    class RrServer{
        on_decide()
    }

    class Scheduler{
        List~Job~ future_jobs
        List~Job~ job_to_add
        String scheduling_algo
        int current_time

        run()
        on_perceive()
        on_decide()
    }

    Server <|-- FIFOServer
    Server <|-- RMSServer
    Server <|-- EDFServer
    Server <|-- RrServer
    Server --o Scheduler: servers
    State_Enum --o Server: state
```

The main structure is the scheduler, which have N servers, each server can be specialized by a scheduling algorithm.  
Each server have his own pending jobs, the only role of the scheduler is to scatter the arriving job into the most appropriate server using server score (as well as managing the overall system power usage).

## **Scheduling Cycle Explained**

### **Stop condition**

There are no future job to come, and all the servers have no jobs remaining. 

Note : An improvement of the current condition would be if the energy cap is exceeded then all servers stops.

### **Part 1 : Scheduler work**

**Scheduler.on_perceive()** : The cycle start with the scheduler, that will look if any future job arrive at the current time, if so it will transfer them in order to scatter them to the server.

**Scheduler.on_decide_and_act()**: For each job to schedule, ask for server score and give the job to the best server.

Score is a function that will evaluate how likely a server want a new jobs, looking at the workload of the server and if he'll most likely miss a deadline. (Note : the score doesn't change between kind of server : FIFO, rmd, rr, EDF... )
```mermaid
stateDiagram-v2
    state Scores {
        direction LR
        count: score = 0
        kstart: relative_time = current_time
        [*] --> count
        count --> kstart
        State forAllJob{

            overloaded: Overloaded! Score += 1000

            state if_state_score <<choice>>
            if_state_score --> overloaded: if relative_time > deadline
            if_state_score --> counttime : else
            overloaded --> counttime
            
            select_earliest_deadline --> kadd
            kadd : relative_time += unit_left/max_freq
            kadd --> if_state_score
            counttime : score += unit_left
        }
        kstart --> forAllJob
        return : return Score
        forAllJob --> return
    }
```
### **Part 2 : Server decide**

In this part of the cycle each server will start by :

**Server.on_decide()** : Which is the only method that change between server kind (FIFO, RMS, Round Robin and EDF), it will choose which job will be executing next in the remaining jobs the server have to execute. 

**Server.on_decide_freq()** : Then the server will choose at which frequency they want to work for this tick. The frequency will be the lowest frequency possible that don't miss a deadline for sure (using a similar method as score)

### **Part 3 : Power check**

Once all the server have chosen their job, the scheduler will check if the system power usage is not exceeding the cap. If so it will shutdown server(s). The shutdown order will be determined with similar criteria as server score.

### **Part 4 : Server work**

The server authorized working, will work for this tick and return metrics to the scheduler

## **Power management**

All the power management was done using what's stated in the labwork. So few assumptions was made : 
 * There are no static power usage on a server -> this mean that the scheduler will always prefer to split the work as much as possible in order to maximize the potential slowdown
 * All server has the same pmax -> this mean that the scheduler don't really have to compare energy weight of a job between 2 server but mainly focus on the potential slowdown

Note : my current system could still be improved by selecting server with the best potential slowdown.

So the power management was mainly done at server level. While choosing frequency the server will slow down as much as possible while also trying to not be overloaded.  
The scheduler will always make that power consumption don't go above the cap. BUT the cap cannot be bellow server pmax, it's a limitation of my current model.  

## **Metrics**

For each server the metrics are : 
 * Power usage : average and max
 * The total Watts the server used
 * How many ticks the server worked
 * How much tick did the server work past a deadline
 * How much unit of work the server did
 * Which job work and which freq was used

Note : An improvement would be to store the max tardiness

For a scheduler : 
 * The energy used over time

## **Result**

The result is the output of the scheduler with the "test1" instance that was given in moodle. For every algorithm the result are show taking into account all the parameter (multi server, multi frequencies, energy aware).  
Quick reminder : dependencies were not used.

### **FIFO**

FIFO power usage over time:  
![FIFO_power](output_metrics/Fifopower.png)

FIFO Energy usage over time :  
![FIFO_energy](output_metrics/fifoenergy.png)

Other metrics :  
```
-----------------------
[SERVER] ID -> 0
Power : 
 | Average -> 8.357915437561442
 | Max -> 22.222222222222218
Work :
 | Tick worked -> 85
 | Unit of work -> 85
 | Work post-deadline -> 0
Energy : 1888.888888888886 Watts
-----------------------
[SERVER] ID -> 1
Power : 
 | Average -> 11.061946902654867
 | Max -> 200.0
Work :
 | Tick worked -> 38
 | Unit of work -> 42
 | Work post-deadline -> 0
Energy : 2500.0 Watts
-----------------------
[SERVER] ID -> 2
Power : 
 | Average -> 1.9665683382497545
 | Max -> 22.222222222222218
Work :
 | Tick worked -> 20
 | Unit of work -> 20
 | Work post-deadline -> 0
Energy : 444.4444444444445 Watts
-----------------------
[SERVER] ID -> 3
Power : 
 | Average -> 6.415929203539823
 | Max -> 200.0
Work :
 | Tick worked -> 17
 | Unit of work -> 21
 | Work post-deadline -> 0
Energy : 1450.0 Watts
[SCHEDULER] Worked :  168  in :  160  ticks, total miss :  0
[SCHEDULER] Energy :  6283.333333333357  Cap was :  10000
```

Scheduling :  
![FIFO_schedul](output_metrics/fifoschedul.png)

Scheduling focused on the start :  
![FIFO_scheduling_zoom](output_metrics/fifofocusschedul.png)

### **Round Robin**

Round robin power usage over time:  
![rr_power](output_metrics/rrpower.png)

Round robin Energy usage over time :  
![rr_energy](output_metrics/rrenergy.png)

Other metrics :  
```
-----------------------
[SERVER] ID -> 0
Power : 
 | Average -> 8.357915437561442
 | Max -> 22.222222222222218
Work :
 | Tick worked -> 85
 | Unit of work -> 85
 | Work post-deadline -> 0
Energy : 1888.888888888886 Watts
-----------------------
[SERVER] ID -> 1
Power : 
 | Average -> 9.513274336283185
 | Max -> 200.0
Work :
 | Tick worked -> 31
 | Unit of work -> 35
 | Work post-deadline -> 0
Energy : 2150.0 Watts
-----------------------
[SERVER] ID -> 2
Power : 
 | Average -> 2.55653883972468
 | Max -> 22.222222222222218
Work :
 | Tick worked -> 26
 | Unit of work -> 26
 | Work post-deadline -> 0
Energy : 577.7777777777777 Watts
-----------------------
[SERVER] ID -> 3
Power : 
 | Average -> 6.415929203539823
 | Max -> 200.0
Work :
 | Tick worked -> 17
 | Unit of work -> 21
 | Work post-deadline -> 1
Energy : 1450.0 Watts
[SCHEDULER] Worked :  167  in :  159  ticks, total miss :  1
[SCHEDULER] Energy :  6066.666666666688  Cap was :  10000
```

Scheduling :  
![rr_energy_scheduling](output_metrics/rrscheduling.png)

Scheduling focused on the start :  
![rr_scheduling_zoom](output_metrics/rrfocusedscheduling.png)

### **RMS**

RMS power usage over time:  
![RMS_power](output_metrics/rmspower.png)

RMS Energy usage over time :  
![RMS_energy](output_metrics/rmsenergy.png)

Other metrics :  
```
-----------------------
[SERVER] ID -> 0
Power : 
 | Average -> 8.357915437561442
 | Max -> 22.222222222222218
Work :
 | Tick worked -> 85
 | Unit of work -> 85
 | Work post-deadline -> 0
Energy : 1888.888888888886 Watts
-----------------------
[SERVER] ID -> 1
Power : 
 | Average -> 11.061946902654867
 | Max -> 200.0
Work :
 | Tick worked -> 38
 | Unit of work -> 42
 | Work post-deadline -> 0
Energy : 2500.0 Watts
-----------------------
[SERVER] ID -> 2
Power : 
 | Average -> 1.9665683382497545
 | Max -> 22.222222222222218
Work :
 | Tick worked -> 20
 | Unit of work -> 20
 | Work post-deadline -> 0
Energy : 444.4444444444445 Watts
-----------------------
[SERVER] ID -> 3
Power : 
 | Average -> 6.415929203539823
 | Max -> 200.0
Work :
 | Tick worked -> 17
 | Unit of work -> 21
 | Work post-deadline -> 0
Energy : 1450.0 Watts
[SCHEDULER] Worked :  168  in :  160  ticks, total miss :  0
[SCHEDULER] Energy :  6283.333333333357  Cap was :  10000
```

Scheduling :  
![RMS_scheduling](output_metrics/rmsscheduling.png)

Scheduling focused on the start :  
![RMS_focused](output_metrics/rmsfocusedscheduling.png)

### **EDF**

EDF power usage over time:  
![EDF_power](output_metrics/edfpower.png)

EDF Energy usage over time :  
![EDF_energy](output_metrics/edfenergy.png)

Other metrics :
```
-----------------------
[SERVER] ID -> 0
Power : 
 | Average -> 8.357915437561442
 | Max -> 22.222222222222218
Work :
 | Tick worked -> 85
 | Unit of work -> 85
 | Work post-deadline -> 0
Energy : 1888.888888888886 Watts
-----------------------
[SERVER] ID -> 1
Power : 
 | Average -> 7.743362831858407
 | Max -> 50.0
Work :
 | Tick worked -> 35
 | Unit of work -> 35
 | Work post-deadline -> 0
Energy : 1750.0 Watts
-----------------------
[SERVER] ID -> 2
Power : 
 | Average -> 2.55653883972468
 | Max -> 22.222222222222218
Work :
 | Tick worked -> 26
 | Unit of work -> 26
 | Work post-deadline -> 0
Energy : 577.7777777777777 Watts
-----------------------
[SERVER] ID -> 3
Power : 
 | Average -> 4.646017699115045
 | Max -> 50.0
Work :
 | Tick worked -> 21
 | Unit of work -> 21
 | Work post-deadline -> 0
Energy : 1050.0 Watts
[SCHEDULER] Worked :  167  in :  167  ticks, total miss :  0
[SCHEDULER] Energy :  5266.666666666678  Cap was :  10000
```

Scheduling :  
![EDF_scheduling](output_metrics/edfscheduling.png)

Scheduling focused on the start :  
![EDF_focused](output_metrics/edffocusedscheduling.png)

### **Comparison**

Using "test1" instance, and the current model in teRMS of energy usage:
 1) EDF is the best algorithm by far
 2) All the other one have similar energy usage (RR is a bit better)

With the given instance no deadline was missed, so we can't compare this part.

### **Power management test**

This test was done using RMS on "test1" instance but reducing power cap to 200. (default was 600) in order to show the shutdown part of the model.

Power usage :  
![cap_power](output_metrics/captestpower.png)

Energy usage :  
![cap_evergy](output_metrics/captestenergy.png)

Other metrics :  
```
-----------------------
[SERVER] ID -> 0
Power : 
 | Average -> 12.192723697148464
 | Max -> 200.0
Work :
 | Tick worked -> 72
 | Unit of work -> 86
 | Work post-deadline -> 3
Energy : 2755.555555555553 Watts
-----------------------
[SERVER] ID -> 1
Power : 
 | Average -> 9.070796460176991
 | Max -> 200.0
Work :
 | Tick worked -> 32
 | Unit of work -> 35
 | Work post-deadline -> 3
Energy : 2050.0 Watts
-----------------------
[SERVER] ID -> 2
Power : 
 | Average -> 6.293018682399214
 | Max -> 200.0
Work :
 | Tick worked -> 21
 | Unit of work -> 32
 | Work post-deadline -> 3
Energy : 1422.2222222222222 Watts
-----------------------
[SERVER] ID -> 3
Power : 
 | Average -> 4.646017699115045
 | Max -> 200.0
Work :
 | Tick worked -> 12
 | Unit of work -> 15
 | Work post-deadline -> 3
Energy : 1050.0 Watts
[SCHEDULER] Worked :  168  in :  137  ticks, total miss :  12
[SCHEDULER] Energy :  7277.777777777797  Cap was :  10000
```

Scheduling :  
![cap_scheduling](output_metrics/cappowerscheduling.png)

Scheduling focused on the start :  
![cap_focused](output_metrics/cappowerfocusedscheduling.png)
