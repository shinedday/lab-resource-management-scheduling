"""
Utils tool for scheduler in order to print the energy related metrics
"""
import matplotlib.pyplot as plt
from typing import List

from scheduler.outputdata import Energy


class EnergyPlotter:
    """
    Class that help to plot energy consumption over time
    """

    def __init__(self, pmax, emax):
        self.energy_list: List[Energy] = []

        self.pmax = pmax
        self.emax = emax

    def print_max_energy(self) -> None:
        """
        Print the energy consumption of the scheduler in the terminal
        :return: None
        """
        total_energy = 0
        for energy in self.energy_list:
            total_energy += energy.total
        print("[SCHEDULER] Energy : ", total_energy, " Cap was : ", self.emax)

    def add_energy(self, energy: Energy) -> None:
        """
        :param energy: Energy
        :return: None
        """
        self.energy_list.append(energy)

    def plot_power(self) -> None:
        """
        Plot all the data gathered in energy_list
        :return: None
        """
        x_axis = []
        y_total_axis = []
        y_server = [[] for _ in range(len(self.energy_list[0].energy))]
        y_max = []
        for energy in self.energy_list:
            x_axis.append(energy.time)
            y_total_axis.append(energy.total)

            for index_server in range(len(energy.energy)):
                y_server[index_server].append(energy.energy[index_server])

            y_max.append(self.pmax)

        plt.plot(x_axis, y_total_axis, linestyle='-', label="TotalPower")
        plt.plot(x_axis, y_max, linestyle='-', label="PowerMax")
        for index_server in range(len(self.energy_list[0].energy)):
            plt.plot(x_axis, y_server[index_server], linestyle='-', label="Server-" + str(index_server))

        plt.title("Power over time", loc="left")
        plt.legend(loc="upper right")
        plt.xlabel("Time")
        plt.ylabel("Watt")

        plt.show()

    def plot_energy(self):

        x_axis = []
        y_axis = []
        y_max = []

        for energy in self.energy_list:
            x_axis.append(energy.time)

            y_max.append(self.emax)

            if len(y_axis) == 0:
                y_axis.append(energy.total)
            else:
                y_axis.append(y_axis[-1] + energy.total)

        plt.plot(x_axis, y_axis, linestyle='-', label="TotalEnergy")
        plt.plot(x_axis, y_max, linestyle='-', label="MaxEnergy")

        plt.title("Energy over time", loc="left")
        plt.legend(loc="upper right")
        plt.xlabel("Time")
        plt.ylabel("Joules")

        plt.show()
