"""
Tools to write outputs in a formatted way.
"""
from typing import List

from scheduler.outputdata import OutputData


class Writer:
    """
    Subclass of scheduler providing tools to write the output file
    """

    def __init__(self, path):
        self.path = path
        self.data: List[OutputData] = []

    def job_done(self, data: OutputData) -> None:
        """
        :param data: job done
        :return: None
        """
        self.data.append(data)

    def write(self) -> None:
        """
        Write all job done in the output file
        Note: Overwrite all data.
        :return: None
        """
        if self.path is None:
            return

        self.merging_jobs()

        print("Write in output file..")
        with open(self.path, 'w') as f:
            f.write("#jobid serverid start end frequency\n")

            for data in self.data:
                f.write(data.write())
        print("Done")

    def merging_jobs(self):
        """
        put back the jobs together in order to have a nicer plot
        :return: None
        """
        job_id1 = 0

        while job_id1 < len(self.data):
            job_id2 = job_id1 + 1

            changed = False

            while job_id2 < len(self.data):

                if self.data[job_id1].same_job(self.data[job_id2]):
                    changed = True
                    if self.data[job_id1].start == self.data[job_id2].end:
                        self.data[job_id1].start = self.data[job_id2].start
                    else:
                        self.data[job_id1].end = self.data[job_id2].end

                    del self.data[job_id2]
                else:
                    job_id2 += 1

            if not changed:
                job_id1 += 1


