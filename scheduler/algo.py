"""
Scheduling algorythm to use
"""
from enum import Enum, auto


class Algo(Enum):
    FIFO = auto()
    EDF = auto()
    RMS = auto()
    ROUNDROBIN = auto()
