"""
Class for formatting output of job done
"""


class OutputData:
    """
    Class that represent 1 line in the output file
    """

    def __init__(self, job_id: int, serv_id: int, start: int, end: int, freq: int, power: float):
        self.job_id = job_id
        self.serv_id = serv_id
        self.start = start
        self.end = end
        self.freq = freq

        self.power = power

    def write(self) -> str:
        """
        :return: str job done for output file
        """
        return str(self.job_id) + " " + \
            str(self.serv_id) + " " + \
            str(self.start) + " " + \
            str(self.end) + " " + \
            str(self.freq) + "\n"

    def same_job(self, other) -> bool:
        """
        compare 2 jobs in order to know if they are the same
        :param other: Job
        :return: Bool
        """
        same_job = (self.job_id == other.job_id) and \
                   (self.serv_id == other.serv_id) and \
                   (self.freq == other.freq) and \
                   (self.start == other.end or self.end == other.start)

        return same_job


class Energy:
    """
    Energy consumption at a given time
    """

    def __init__(self, e_servers, time):
        self.time = time
        self.energy = e_servers
        self.total = sum(self.energy)
