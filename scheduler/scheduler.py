from math import ceil
from typing import List

from instance.job import Job
from instance.serverdata import ServerData
from scheduler.algo import Algo
from scheduler.energyplotter import EnergyPlotter
from scheduler.outputdata import Energy
from scheduler.writer import Writer
from server.edf import EdfServer
from server.fifo import FifoServer
from server.rms import RmsServer
from server.rr import RrServer
from server.server import Server
from server.state import State


class Scheduler(Writer, EnergyPlotter):

    def __init__(
            self,
            jobs: List[Job],
            scheduling_algo: Algo,
            servers: List[ServerData],
            pmax: int,
            emax: int,
            path: str = None
    ):
        Writer.__init__(self, path)
        EnergyPlotter.__init__(self, pmax, emax)

        self.jobs: List[Job] = jobs
        self.current_time: int = 0

        self.nbr_server = len(servers)

        self.servers: List[Server] = []
        self.scheduling_algo = scheduling_algo
        self.create_servers(servers)

        self.job_to_add: List[Job] = []
        self.server_score: List[int] = [-1 for _ in range(self.nbr_server)]

    def create_servers(self, servers) -> None:
        """
        create servers according to the wanted scheduling algorithm
        :return: None
        """
        if self.scheduling_algo == Algo.FIFO:
            for server in servers:
                self.servers.append(FifoServer(server.identifier, server.freq, self))
        elif self.scheduling_algo == Algo.EDF:
            for server in servers:
                self.servers.append(EdfServer(server.identifier, server.freq, self))
        elif self.scheduling_algo == Algo.RMS:
            for server in servers:
                self.servers.append(RmsServer(server.identifier, server.freq, self))
        elif self.scheduling_algo == Algo.ROUNDROBIN:
            for server in servers:
                self.servers.append(RrServer(server.identifier, server.freq, self))
        else:
            raise Exception("Choose a correct algorithm")

    def on_perceive(self) -> None:
        """
        look for job in jobs if there are new jobs
        if so, gather server scores
        :return: None
        """

        self.job_to_add = []
        job_id = 0
        while job_id < len(self.jobs):
            if self.jobs[job_id].arrival <= self.current_time:
                self.job_to_add.append(self.jobs[job_id])
                del self.jobs[job_id]
            else:
                job_id += 1

    def on_decide_and_act(self) -> None:
        """
        give the job to add to the servers according to the scores
        :return:
        """
        for job in self.job_to_add:

            for id_server in range(self.nbr_server):
                self.server_score[id_server] = self.servers[id_server].score()

            server_index = self.server_score.index(min(self.server_score))
            print("Job added to server :", server_index)

            self.servers[server_index].add_job(job)

    def run(self) -> None:
        """
        self on perceive and on decide

        then for all server, on decide and on act

        :return: None
        """

        while not self.is_finished():

            print("Cycle : ", self.current_time)

            self.on_perceive()
            self.on_decide_and_act()

            energy = []

            for server in self.servers:
                server.on_decide()

                server.on_decide_freq()

            can_work = self.can_server_work()

            for server_id in range(len(self.servers)):
                result = None
                if can_work[server_id]:
                    result = self.servers[server_id].on_act()
                if result is not None:
                    energy.append(result.power)
                    self.job_done(result)
                else:
                    energy.append(0)

            self.add_energy(Energy(energy, self.current_time))
            self.current_time += 1

    def can_server_work(self) -> List[bool]:

        scores = []
        for server_id in range(len(self.servers)):
            scores.append(
                [
                    self.servers[server_id].can_shut_down(),
                    ceil(self.servers[server_id].get_power()),
                    self.servers[server_id].identifier,
                    server_id
                ]
            )

        power = sum([x[1] for x in scores])

        while power > self.pmax:
            server_index = 0

            for index in range(len(scores)):
                if scores[server_index][0] > scores[index][0]:
                    server_index = index

            del scores[server_index]
            power = sum([x[1] for x in scores])

        can_work = [False for _ in range(len(self.servers))]

        for score in scores:
            can_work[score[3]] = True
        return can_work

    def print_report(self) -> None:
        """
        print how well the execution went
        :return: None
        """

        total_work = 0
        total_time = 0
        total_miss = 0

        for server in self.servers:
            print(
                """-----------------------
[SERVER] ID -> {identifier}
Power : 
 | Average -> {powav}
 | Max -> {powmax}
Work :
 | Tick worked -> {tick}
 | Unit of work -> {unit}
 | Work post-deadline -> {deadline}
Energy : {energy} Watts""".format(
                    identifier=server.identifier,
                    powav=server.energy / self.current_time,
                    powmax=server.maxpower,
                    tick=server.time_worked,
                    unit=server.amount_of_work,
                    energy=server.energy,
                    deadline=server.deadline_missed
                )
            )

            total_time += server.time_worked
            total_work += server.amount_of_work
            total_miss += server.deadline_missed
        print("[SCHEDULER] Worked : ", total_work, " in : ", total_time, " ticks, total miss : ", total_miss)
        self.print_max_energy()

    def is_finished(self) -> bool:
        """
        :return: true if every job are done
        """
        if len(self.jobs) > 0:
            return False
        for server in self.servers:
            if server.state == State.WORKING:
                return False
        return True
